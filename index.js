let express = require ('express');
let app = express();
let mongoose = require ('mongoose');

const PORT = 5000;

let taskRoutes = require('./Routes/taskRoutes')

app.use(express.json());
app.use(express.urlencoded({extended:true}));

mongoose.connect("mongodb+srv://daliuz:517892021@cluster0.mlkat.mongodb.net/s31?retryWrites=true&w=majority",
	{
		useNewUrlParser:true,
		useUnifiedTopology:true
	}
).then(()=> { 
	console.log("Successfully Connected to Database!");
}).catch((error)=> {
	console.log(error);
});

app.use("/", taskRoutes);


app.listen(PORT, ()=> console.log(`Server Running on port ${PORT}`));